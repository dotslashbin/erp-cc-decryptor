﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Win32;
using System;
using System.Security.Cryptography;
using System.Text;
using ERP;
using ERP.Providers;
using ErpProtect;
using Decryptor.Models;
using System.Configuration;
using System.Web.Security;
using System.Data.SqlClient;


namespace Decryptor.Controllers
{
    public class MainController : Controller
    {
        // Members

        // protected DevScratchEntities decryptorModel = new DevScratchEntities(); 
        protected PreviousDB previousDBModel = new PreviousDB();
        // protected MigrationEntities migratedDBModel = new MigrationEntities(); 
        protected MigrationEntities MigrationModelEntities = new MigrationEntities();


        // GET: /Main/
        public ActionResult Index()
        {



            return View();
        }

        [HttpPost]
        public ActionResult run_update(string limit, string offset)
        {

            List<Erp_ProfileUnwrap> Profiles = previousDBModel.Erp_ProfileUnwrap.OrderBy(foo => foo.UserId).Skip(Convert.ToInt32(offset)).Take(Convert.ToInt32(limit)).ToList();

            SqlConnection SQLConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionForUpdates"].ConnectionString);

            SQLConnection.Open(); 

            foreach (var profile in Profiles)
            {

                user userToUpdate = MigrationModelEntities.users.Where(uid => uid.user_id == profile.UserId).FirstOrDefault(); 

                aspnet_Users aspnet_user = previousDBModel.aspnet_Users.Where(foo => foo.UserId == userToUpdate.user_id).FirstOrDefault();

                

                

                /** Updating Password **/
                if (aspnet_user != null)
                {
                    if (Membership.GetUser(aspnet_user.UserName.ToString()) != null)
                    {
                        var user = Membership.GetUser(aspnet_user.UserName.ToString());
                        int userId = Convert.ToInt32(user.ProviderUserKey);
                        MembershipUser mu = ((ERP.Providers.SqlMembershipProvider)Membership.Provider).GetUser(userId, false);

                        // userToUpdate.password = mu.GetPassword();
                        SqlCommand updatePassword = new SqlCommand("UPDATE [Migration].[dbo].[users] SET password='" + mu.GetPassword() + "' WHERE id=" + userToUpdate.id, SQLConnection);
                        updatePassword.ExecuteScalar();
                        
                    }
                }

                /** Updating Credit card number **/
                if (userToUpdate.cc_encoded_number != null)
                {
                    var encryptedCode = userToUpdate.cc_encoded_number;
                    var decodedCC = Encoding.ASCII.GetString(ERP.SecurityUtil.DecodeValue(Convert.FromBase64String(encryptedCode)));

                    //userToUpdate.cc_number = decodedCC;

                    SqlCommand updateCC = new SqlCommand("UPDATE [Migration].[dbo].[users] SET cc_number='" + decodedCC + "' WHERE id=" + userToUpdate.id, SQLConnection);
                    updateCC.ExecuteScalar();
                }

            }

            SQLConnection.Close(); 

            //string sql = "";
            //sql += "INSERT [Membership].[dbo].[AdminNotes] ( [userId], [notes], [dateNoteAdded], [whoAddedNote])";
            //sql += "VALUES ( @UserId, @Notes, @DateTime, @AddedBy)";
            

            // MigrationModelEntities.SaveChanges();

            return View();
        }

    }
}




